FROM python:3

EXPOSE 5002

RUN mkdir /inventory_search
WORKDIR /inventory_search

COPY ./requirements.txt /inventory_search/requirements_inv_db.txt
RUN pip install -r requirements_inv_db.txt

COPY . /inventory_search
CMD ["python", "inventory_search.py"]