from flask import Flask
from flask_restful import Api, Resource, reqparse
import mysql.connector


app = Flask(__name__)
api = Api(app)

def connect_db():
    """:return a connection to the inventory_db"""

    #MYSQL database config for connection
    mysql_config = {
        'user': 'root',
        'password': 'root',
        'host': 'inventory_db',
        'database': 'inventory'
    }

    # if database is not accepting connections, keep trying till available
    connected = False
    while connected is False:
        try:
            con = mysql.connector.connect(**mysql_config)
            connected = True

        except Exception as e:
            continue

    return con

class Search(Resource):
    def post(self):
        """POST: per_page, offset, search_string
        Queries the database for a search_string and returns results in a python Dictionary"""

        con = connect_db()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('per_page', type=int, help='Items allowed for pagination')
        parser.add_argument('offset', type=int, help='Offset to grab db rows from for pagination')
        parser.add_argument('search_string', type=str, help='Query String')
        args = parser.parse_args()

        per_page = args['per_page']
        offset = args['offset']
        search_string = args['search_string']
        # add the wilcard characters to the search_string to search for items containing the search string not literals
        search_string = '%' + search_string + '%'

        sql = "SELECT * FROM items WHERE item_name LIKE %s LIMIT %s,%s"

        try:
            cursor.execute(sql, (search_string, offset, per_page))
        except Exception as e:
            return {'error': str(e)}

        search_results = cursor.fetchall()

        cursor.close()
        con.close()

        return search_results

class GetAllListings(Resource):
    def post(self):
        """POST: per_page, offset
        Method to grab all items from the database, selects all rows and returns in dictionary"""
        con = connect_db()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('per_page', type=int, help='Items allowed for pagination')
        parser.add_argument('offset', type=int, help='Offset to grab db rows from for pagination')
        args = parser.parse_args()

        per_page = args['per_page']
        offset = args['offset']

        sql = "SELECT * FROM items LIMIT %s,%s"

        try:
            cursor.execute(sql, (offset, per_page))
        except Exception as e:
            return {'error': str(e)}

        search_results = cursor.fetchall()

        cursor.close()
        con.close()

        return search_results

class Count_All(Resource):
    def get(self):
        """GET: no args
        Get a count of how many items are being returned by GetAllListings, for use in pagination"""
        con = connect_db()
        cursor = con.cursor(dictionary=True)

        sql = "SELECT count(itemID) as count FROM items"

        try:
            cursor.execute(sql)
        except Exception as e:
            return {'error': str(e)}

        search_results = cursor.fetchall()

        cursor.close()
        con.close()

        return search_results

class Count_Search(Resource):
    def post(self):
        """POST: search_string
        Get a count of how many results returned by Search"""
        con = connect_db()
        cursor = con.cursor(dictionary=True)

        parser = reqparse.RequestParser()
        parser.add_argument('search_string', type=str, help='Query String')
        args = parser.parse_args()

        search_string = args['search_string']
        search_string = '%' + search_string + '%'

        sql = "SELECT count(itemID) as count FROM items WHERE item_name LIKE %s"

        try:
            cursor.execute(sql, (search_string,))
        except Exception as e:
            return {'error': str(e)}

        search_results = cursor.fetchall()

        cursor.close()
        con.close()

        return search_results


# Add the api routes to the app
api.add_resource(Search, '/Search')
api.add_resource(GetAllListings, '/allListings')
api.add_resource(Count_All, '/count_allListings')
api.add_resource(Count_Search, '/count_Search')


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5002)
